module UpdatedSvg exposing (href)

import VirtualDom exposing (Attribute, attribute)


href : String -> Attribute msg
href url =
    -- xlink:href is deprecated in SVG, but elm/svg doesn’t provide
    -- plain ‘href’, so we do that here.
    attribute "href" url

port module Main exposing (main)

import Browser
import Browser.Events exposing (onAnimationFrameDelta, onKeyDown)
import Color exposing (Color)
import Grid exposing (Grid)
import Html
import Json.Decode as Decode
import Json.Encode as Encode
import Random
import Recurse exposing (Recursion(..), recurse)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import UpdatedSvg exposing (..)



-- MAIN


main : Program Encode.Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { boardState : BoardState
    , score : Int
    , highScore : Int
    , gameIsOver : Bool
    }


type BoardState
    = Static { tiles : Grid (Maybe Int) }
    | Sliding
        { tiles : Grid (Maybe SlidingTile)
        , amount : Float
        , step : Int
        , direction : Direction
        }


type alias SlidingTile =
    { rank : Int
    , didCombine : Bool
    }


type Direction
    = Left
    | Right
    | Up
    | Down


init : Encode.Value -> ( Model, Cmd Msg )
init flags =
    let
        highScore =
            Result.withDefault 0
                (Decode.decodeValue
                    (Decode.field "highScore" Decode.int)
                    flags
                )
    in
    ( { boardState = Static { tiles = initTiles }
      , score = 0
      , highScore = highScore
      , gameIsOver = False
      }
    , newTileCmd initTiles
    )


initTiles : Grid (Maybe Int)
initTiles =
    List.repeat numRows (List.repeat numRows Nothing)


numRows : Int
numRows =
    4



-- PORTS


port setStorage : Encode.Value -> Cmd msg



-- UPDATE


type Msg
    = StartSlide Direction
    | AnimationFrame Float
    | AddTile Int Int Int
    | NewGame
    | NothingHappened


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StartSlide direction ->
            let
                newBoardState =
                    case model.boardState of
                        Static { tiles } ->
                            Sliding
                                { tiles = Grid.map rankToSlidingTile tiles
                                , amount = 0.0
                                , step = 0
                                , direction = direction
                                }

                        Sliding _ ->
                            model.boardState
            in
            ( { model | boardState = newBoardState }, Cmd.none )

        AnimationFrame dt ->
            case model.boardState of
                Sliding { tiles, amount, step, direction } ->
                    let
                        newAmount =
                            amount
                                + slideAmountStep (toFloat step + amount) dt
                    in
                    if newAmount < 1.0 then
                        let
                            newBoardState =
                                Sliding
                                    { tiles = tiles
                                    , amount = newAmount
                                    , step = step
                                    , direction = direction
                                    }
                        in
                        ( { model | boardState = newBoardState }, Cmd.none )

                    else
                        -- We have slid a whole step
                        let
                            newTiles =
                                withTransformedTiles
                                    direction
                                    (List.map collapseCol)
                                    tiles

                            newScore =
                                model.score + scoreIncrement tiles newTiles

                            newHighScore =
                                Basics.max newScore model.highScore

                            setHighScoreCmd =
                                if newHighScore > model.highScore then
                                    storeHighScore model.highScore

                                else
                                    Cmd.none
                        in
                        if anyTilesWillMove newTiles direction then
                            -- This is not the last step
                            let
                                newBoardState =
                                    Sliding
                                        { tiles = newTiles
                                        , amount = newAmount - 1.0
                                        , step = step + 1
                                        , direction = direction
                                        }
                            in
                            ( { model
                                | boardState = newBoardState
                                , score = newScore
                                , highScore = newHighScore
                              }
                            , setHighScoreCmd
                            )

                        else
                            -- Sliding is finished
                            let
                                staticTiles =
                                    Grid.map (Maybe.map .rank) newTiles
                            in
                            ( { model
                                | boardState = Static { tiles = staticTiles }
                                , score = newScore
                                , highScore = newHighScore
                              }
                            , Cmd.batch
                                [ setHighScoreCmd
                                , newTileCmd
                                    (Grid.map (Maybe.map .rank) newTiles)
                                ]
                            )

                Static _ ->
                    ( model, Cmd.none )

        AddTile colNum rowNum rank ->
            ( case model.boardState of
                Static { tiles } ->
                    let
                        newTiles =
                            Grid.set colNum rowNum (Just rank) tiles
                    in
                    if anyMovesPossible newTiles then
                        { model | boardState = Static { tiles = newTiles } }

                    else
                        { model
                            | boardState = Static { tiles = newTiles }
                            , gameIsOver = True
                        }

                Sliding _ ->
                    -- Can’t add a tile while tiles are sliding
                    model
            , Cmd.none
            )

        NewGame ->
            ( { model
                | boardState = Static { tiles = initTiles }
                , score = 0
                , gameIsOver = False
              }
            , newTileCmd initTiles
            )

        NothingHappened ->
            ( model, Cmd.none )


rankToSlidingTile : Maybe Int -> Maybe SlidingTile
rankToSlidingTile tile =
    Maybe.map
        (\rank -> { rank = rank, didCombine = False })
        tile


slideAmountStep : Float -> Float -> Float
slideAmountStep amount dt =
    let
        dAmount =
            dt / (0.5 * (1.0 + sqrt 5.0) * slideTime)
    in
    toFloat (numRows - 1) * (2.0 * amount * dAmount + dAmount ^ 2 + dAmount)


scoreIncrement : Grid (Maybe SlidingTile) -> Grid (Maybe SlidingTile) -> Int
scoreIncrement prevTiles tiles =
    Grid.sum (Grid.map2 maybeTileScore prevTiles tiles)


maybeTileScore : Maybe SlidingTile -> Maybe SlidingTile -> Int
maybeTileScore maybePrevTile maybeTile =
    case maybeTile of
        Just { rank, didCombine } ->
            if didCombine && not (maybeTileDidCombine maybePrevTile) then
                fibonacci rank

            else
                0

        Nothing ->
            0


maybeTileDidCombine : Maybe SlidingTile -> Bool
maybeTileDidCombine maybeTile =
    Maybe.withDefault False (Maybe.map .didCombine maybeTile)


fibonacci : Int -> Int
fibonacci n =
    case n of
        0 ->
            1

        _ ->
            Tuple.second
                (List.foldr (<|)
                    ( 1, 2 )
                    (List.repeat (n - 1) (\( a, b ) -> ( b, a + b )))
                )


storeHighScore : Int -> Cmd Msg
storeHighScore score =
    setStorage (Encode.object [ ( "highScore", Encode.int score ) ])


anyTilesWillMove : Grid (Maybe SlidingTile) -> Direction -> Bool
anyTilesWillMove grid movementDirection =
    List.any .isMoving
        (Grid.filterToList identity
            (withTransformedTiles movementDirection convertToDisplayTiles grid)
        )


newTileCmd : Grid (Maybe a) -> Cmd Msg
newTileCmd tiles =
    case coordsOfFreeSpaces tiles of
        firstFreeSpace :: otherFreeSpaces ->
            Random.generate (\( col, row, rank ) -> AddTile col row rank)
                (Random.map2
                    (\( col, row ) rank -> ( col, row, rank ))
                    (Random.uniform firstFreeSpace otherFreeSpaces)
                    (Random.int 0 2)
                )

        [] ->
            Cmd.none


coordsOfFreeSpaces : Grid (Maybe a) -> List ( Int, Int )
coordsOfFreeSpaces =
    Grid.indexedFilterToList
        (\col row maybeRank ->
            case maybeRank of
                Just _ ->
                    Nothing

                Nothing ->
                    Just ( col, row )
        )


withTransformedTiles : Direction -> (Grid a -> Grid b) -> Grid a -> Grid b
withTransformedTiles direction f =
    -- f is a function that maps a grid of values to another grid.
    -- This function adapts f to operate on a transformed version of
    -- the grid, where the transformation is given by ‘direction’.
    -- So, whatever direciton f thinks is “up” is mapped to the given
    -- direction.
    case direction of
        Up ->
            f

        Down ->
            Grid.reverseCols >> f >> Grid.reverseCols

        Left ->
            Grid.transpose >> f >> Grid.transpose

        Right ->
            Grid.transpose
                >> Grid.reverseCols
                >> f
                >> Grid.reverseCols
                >> Grid.transpose


type alias DisplayTile =
    { label : Int
    , isMoving : Bool
    }


convertToDisplayTiles : Grid (Maybe SlidingTile) -> Grid (Maybe DisplayTile)
convertToDisplayTiles =
    List.map convertColToDisplayTiles


convertColToDisplayTiles : List (Maybe SlidingTile) -> List (Maybe DisplayTile)
convertColToDisplayTiles col =
    case col of
        tile :: restOfTiles ->
            recurse
                { isMoving = False
                , maybeTile0 = tile
                , remainingTiles = restOfTiles
                , reversedResult = []
                }
                (\{ isMoving, maybeTile0, remainingTiles, reversedResult } ->
                    case remainingTiles of
                        maybeTile1 :: newRemainingTiles ->
                            let
                                ( newIsMoving, tile0IsMoving ) =
                                    case ( maybeTile0, maybeTile1 ) of
                                        ( Just tile0, Just tile1 ) ->
                                            if tilesWillCombine tile0 tile1 then
                                                ( True, False )

                                            else
                                                ( isMoving, isMoving )

                                        _ ->
                                            ( True, isMoving )
                            in
                            Recurse
                                { isMoving = newIsMoving
                                , maybeTile0 = maybeTile1
                                , remainingTiles = newRemainingTiles
                                , reversedResult =
                                    convertToDisplayTile
                                        tile0IsMoving
                                        maybeTile0
                                        :: reversedResult
                                }

                        [] ->
                            Stop
                                (List.reverse
                                    (convertToDisplayTile isMoving maybeTile0
                                        :: reversedResult
                                    )
                                )
                )

        [] ->
            []


convertToDisplayTile : Bool -> Maybe SlidingTile -> Maybe DisplayTile
convertToDisplayTile isMoving =
    Maybe.map
        (\tile ->
            { label = fibonacci tile.rank
            , isMoving = isMoving
            }
        )


collapseCol : List (Maybe SlidingTile) -> List (Maybe SlidingTile)
collapseCol =
    foldPairs collapseTilePair


foldPairs : (a -> a -> ( a, a )) -> List a -> List a
foldPairs f list =
    -- Apply f to each (overlapping) pair of successive elements from
    -- ‘list’.  This is best explained with a diagram:
    -- O─🮢
    -- O──f─🮢
    -- O─────f─🮢
    -- O────────f── result
    case list of
        firstItem :: restOfList ->
            List.foldl
                (\item ( prevItem, newList ) ->
                    let
                        ( nextItem0, nextItem1 ) =
                            f prevItem item
                    in
                    ( nextItem1, nextItem0 :: newList )
                )
                ( firstItem, [] )
                restOfList
                |> (\( last, reversedRest ) -> last :: reversedRest)
                |> List.reverse

        [] ->
            []


collapseTilePair :
    Maybe SlidingTile
    -> Maybe SlidingTile
    -> ( Maybe SlidingTile, Maybe SlidingTile )
collapseTilePair maybeTile0 maybeTile1 =
    case ( maybeTile0, maybeTile1 ) of
        ( Nothing, Just tile1 ) ->
            ( Just tile1, Nothing )

        ( Just tile0, Just tile1 ) ->
            -- If the previous tile had slid, then maybeTile0 would be
            -- Nothing.  Therefore, no tiles to the left have slid.
            if tilesWillCombine tile0 tile1 then
                ( Just
                    { rank = Basics.max tile0.rank tile1.rank + 1
                    , didCombine = True
                    }
                , Nothing
                )

            else
                ( Just tile0, Just tile1 )

        _ ->
            ( maybeTile0, maybeTile1 )


tilesWillCombine : SlidingTile -> SlidingTile -> Bool
tilesWillCombine tile0 tile1 =
    (abs (tile0.rank - tile1.rank) == 1 || tile0.rank == 0 && tile1.rank == 0)
        && not (tile0.didCombine || tile1.didCombine)


anyMovesPossible : Grid (Maybe Int) -> Bool
anyMovesPossible grid =
    let
        slidingTiles =
            Grid.map rankToSlidingTile grid
    in
    [ Left, Right, Up, Down ]
        |> List.map (anyTilesWillMove slidingTiles)
        |> List.any identity



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.gameIsOver then
        onKeyDown (Decode.succeed NewGame)

    else
        case model.boardState of
            Sliding _ ->
                onAnimationFrameDelta AnimationFrame

            Static _ ->
                onKeyDown keyDecoder


keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map toDirection (Decode.field "key" Decode.string)


toDirection : String -> Msg
toDirection string =
    case string of
        "ArrowLeft" ->
            StartSlide Left

        "a" ->
            StartSlide Left

        "ArrowRight" ->
            StartSlide Right

        "d" ->
            StartSlide Right

        "ArrowUp" ->
            StartSlide Up

        "w" ->
            StartSlide Up

        "ArrowDown" ->
            StartSlide Down

        "s" ->
            StartSlide Down

        _ ->
            NothingHappened



-- VIEW


view : Model -> Html.Html Msg
view model =
    let
        viewWidthStr =
            String.fromFloat viewWidth

        viewHeightStr =
            String.fromFloat viewHeight

        highestRank =
            List.maximum
                (case model.boardState of
                    Static { tiles } ->
                        Grid.filterToList identity tiles

                    Sliding { tiles } ->
                        Grid.filterToList (Maybe.map .rank) tiles
                )

        highestRankToShow =
            Basics.max 2 (Maybe.withDefault 0 highestRank)
    in
    svg
        [ width viewWidthStr
        , height viewHeightStr
        , viewBox
            (String.fromFloat -boardMargin
                ++ " "
                ++ String.fromFloat -boardMargin
                ++ " "
                ++ viewWidthStr
                ++ " "
                ++ viewHeightStr
            )
        ]
        (defs [] svgDefs
            :: board model.boardState
            :: g
                [ transform
                    ("translate("
                        ++ String.fromFloat (boardSize + panelMargin)
                        ++ ", 0)"
                    )
                ]
                [ scoreView model.score
                    [ transform
                        ("translate(0, "
                            ++ String.fromFloat
                                (0.5 * tileMargin + 2.0 * tileSpacing)
                            ++ ")"
                        )
                    ]
                , highScoreView model.highScore
                    [ transform
                        ("translate(0, "
                            ++ String.fromFloat
                                (0.5 * tileMargin + 0.3 * tileSpacing)
                            ++ ")"
                        )
                    ]
                , fibonacciView highestRankToShow
                    [ transform
                        ("translate(0, "
                            ++ String.fromFloat
                                (0.5 * tileMargin + (3.0 * tileSpacing))
                            ++ ")"
                        )
                    ]
                ]
            :: (if model.gameIsOver then
                    [ gameOverView ]

                else
                    []
               )
        )


svgDefs : List (Svg Msg)
svgDefs =
    [ linearGradient
        [ id "tileVerticalShading"
        , gradientTransform "rotate(90)"
        ]
        (verticalShadingStops tileColor)
    , linearGradient
        [ id "slotVerticalShading"
        , x1 "50%"
        , y1 "100%"
        , x2 "50%"
        , y2 "0%"
        ]
        (verticalShadingStops slotColor)
    , radialGradient
        [ id "tileCornerGradient"
        , r "100%"
        , cx "100%"
        , cy "100%"
        ]
        (edgeGradientStops tileColor)
    , linearGradient
        [ id "tileEdgeGradient"
        , x1 "0.5"
        , y1 "1"
        , x2 "0.5"
        , y2 "0"
        ]
        (edgeGradientStops tileColor)
    , radialGradient
        [ id "slotCornerGradient"
        , r "100%"
        , cx "100%"
        , cy "100%"
        ]
        (edgeGradientStops slotColor)
    , linearGradient
        [ id "slotEdgeGradient"
        , x1 "0.5"
        , y1 "1"
        , x2 "0.5"
        , y2 "0"
        ]
        (edgeGradientStops slotColor)
    , Svg.filter
        [ id "shadowBlur"
        , filterUnits "userSpaceOnUse"
        , x (String.fromFloat -tileDepth)
        , y (String.fromFloat -tileDepth)
        , width (String.fromFloat (tileSize + 2.0 * tileDepth))
        , height (String.fromFloat (tileSize + 2.3 * tileDepth))
        ]
        [ feGaussianBlur
            [ stdDeviation (String.fromFloat (0.3 * tileDepth)) ]
            []
        ]
    , Svg.filter
        [ id "bigShadowBlur"
        , filterUnits "userSpaceOnUse"
        , x (String.fromFloat (-2.0 * boardDepth))
        , y (String.fromFloat (-2.0 * boardDepth))
        , width (String.fromFloat (boardSize + 4.0 * boardDepth))
        , height
            (String.fromFloat
                (boardSize + 4.0 * boardDepth + boardShadowExtension)
            )
        ]
        [ feGaussianBlur
            [ stdDeviation (String.fromFloat boardDepth) ]
            []
        ]
    , rect
        [ id "tileShadow"
        , x "0"
        , y "0"
        , width (String.fromFloat tileSize)
        , height (String.fromFloat (tileSize + tileShadowExtension))
        , rx (String.fromFloat tileCornerRadius)
        , Svg.Attributes.filter "url(#shadowBlur)"
        , opacity "0.5"
        ]
        []
    , g
        [ id "tileShading" ]
        [ tileShadingCorner "tileCornerGradient" 0.0
        , tileShadingCorner "tileCornerGradient" 90.0
        , tileShadingCorner "tileCornerGradient" 180.0
        , tileShadingCorner "tileCornerGradient" 270.0
        , tileShadingEdge "tileEdgeGradient" 0.0
        , tileShadingEdge "tileEdgeGradient" 90.0
        , tileShadingEdge "tileEdgeGradient" 180.0
        , tileShadingEdge "tileEdgeGradient" 270.0
        ]
    , g
        [ id "slotShading" ]
        [ tileShadingCorner "slotCornerGradient" 0.0
        , tileShadingCorner "slotCornerGradient" 90.0
        , tileShadingCorner "slotCornerGradient" 180.0
        , tileShadingCorner "slotCornerGradient" 270.0
        , tileShadingEdge "slotEdgeGradient" 0.0
        , tileShadingEdge "slotEdgeGradient" 90.0
        , tileShadingEdge "slotEdgeGradient" 180.0
        , tileShadingEdge "slotEdgeGradient" 270.0
        ]
    , g
        [ id "slot" ]
        [ rect
            [ x "0"
            , y "0"
            , width (String.fromFloat tileSize)
            , height (String.fromFloat tileSize)
            , rx (String.fromFloat tileCornerRadius)
            , fill "url(#slotVerticalShading)"
            ]
            []
        , use [ href "#slotShading" ] []
        ]
    ]


verticalShadingStops : Color -> List (Svg Msg)
verticalShadingStops color =
    [ stop
        [ offset "0"
        , stopColor (Color.toString color)
        ]
        []
    , stop
        [ offset (String.fromFloat (0.4 * tileDepth / tileSize))
        , stopColor (Color.toString (Color.mul 1.5 color))
        ]
        []
    , stop
        [ offset (String.fromFloat (tileDepth / tileSize))
        , stopColor (Color.toString (Color.mul 1.2 color))
        ]
        []
    , stop
        [ offset "0.5"
        , stopColor (Color.toString color)
        ]
        []
    , stop
        [ offset (String.fromFloat (1.0 - tileDepth / tileSize))
        , stopColor (Color.toString (Color.mul 0.8 color))
        ]
        []
    , stop
        [ offset "1"
        , stopColor (Color.toString (Color.mul 0.5 color))
        ]
        []
    ]


edgeGradientStops : Color -> List (Svg Msg)
edgeGradientStops color =
    let
        edgeColor =
            Color.mul 0.2 { color | a = 0.5 }

        interiorColor =
            { edgeColor | a = 0.0 }
    in
    List.map
        (\x ->
            stop
                [ stopColor (Color.interpString interiorColor edgeColor x)
                , offset
                    (String.fromFloat
                        ((x ^ 0.5)
                            * (tileDepth / tileCornerRadius)
                            + (1.0 - tileDepth / tileCornerRadius)
                        )
                    )
                ]
                []
        )
        (floatRange 0.0 1.0 5)


floatRange : Float -> Float -> Int -> List Float
floatRange start end numSteps =
    List.range 0 numSteps
        |> List.map (\n -> start + toFloat n * (end - start) / toFloat numSteps)


tileShadingCorner : String -> Float -> Svg Msg
tileShadingCorner gradient rotateAngle =
    let
        sizeStr =
            String.fromFloat tileCornerRadius
    in
    Svg.path
        [ fill ("url(#" ++ gradient ++ ")")
        , d
            (("M " ++ sizeStr ++ ",0 ")
                ++ ("A " ++ sizeStr ++ "," ++ sizeStr)
                ++ (" 90 0 0 0," ++ sizeStr)
                ++ ("L " ++ sizeStr ++ "," ++ sizeStr)
                ++ "Z"
            )
        , rotateAroundTileCenter rotateAngle
        ]
        []


tileShadingEdge : String -> Float -> Svg Msg
tileShadingEdge gradient rotateAngle =
    rect
        [ fill ("url(#" ++ gradient ++ ")")
        , x (String.fromFloat tileCornerRadius)
        , y "0"
        , width (String.fromFloat (tileSize - 2.0 * tileCornerRadius))
        , height (String.fromFloat tileCornerRadius)
        , rotateAroundTileCenter rotateAngle
        ]
        []


rotateAroundTileCenter : Float -> Attribute Msg
rotateAroundTileCenter angle =
    transform
        ("rotate("
            ++ String.fromFloat angle
            ++ ", "
            ++ String.fromFloat (tileSize / 2.0)
            ++ ", "
            ++ String.fromFloat (tileSize / 2.0)
            ++ ")"
        )


board : BoardState -> Svg Msg
board state =
    g
        []
        (List.append
            (boardShadow
                :: boardBackground
                :: List.concatMap
                    (\row ->
                        List.map
                            (\col -> slot row col)
                            (List.range 0 (numRows - 1))
                    )
                    (List.range 0 (numRows - 1))
            )
            (tilesView state)
        )


boardShadow : Svg Msg
boardShadow =
    rect
        [ x "0"
        , y "0"
        , width (String.fromFloat boardSize)
        , height (String.fromFloat (boardSize + boardDepth))
        , rx (String.fromFloat (tileCornerRadius + tileMargin))
        , fill "black"
        , opacity "0.5"
        , Svg.Attributes.filter "url(#bigShadowBlur)"
        ]
        []


boardBackground : Svg Msg
boardBackground =
    rect
        [ x "0"
        , y "0"
        , width (String.fromFloat boardSize)
        , height (String.fromFloat boardSize)
        , rx (String.fromFloat (tileCornerRadius + tileMargin))
        , fill "#AAAAAA"
        ]
        []


slot : Int -> Int -> Svg Msg
slot col row =
    g
        [ transform
            ("translate("
                ++ String.fromFloat (tileMargin + toFloat col * tileSpacing)
                ++ ", "
                ++ String.fromFloat (tileMargin + toFloat row * tileSpacing)
                ++ ")"
            )
        ]
        [ use [ href "#slot" ] [] ]


scoreView : Int -> List (Attribute Msg) -> Svg Msg
scoreView score attrs =
    text_
        (x (String.fromFloat panelWidth)
            :: fill "#334833"
            :: textAnchor "end"
            :: fontSize (String.fromFloat (0.75 * tileSize))
            :: fontFamily "serif"
            :: fontWeight "bold"
            :: attrs
        )
        [ text (String.fromInt score) ]


highScoreView : Int -> List (Attribute Msg) -> Svg Msg
highScoreView score attrs =
    let
        textSize =
            0.33 * tileSpacing
    in
    g
        attrs
        [ text_
            [ y (String.fromFloat textSize)
            , fontSize (String.fromFloat (0.5 * textSize))
            , fontFamily "serif"
            ]
            [ text "High Score" ]
        , text_
            [ x (String.fromFloat panelWidth)
            , y (String.fromFloat textSize)
            , textAnchor "end"
            , fontSize (String.fromFloat textSize)
            , fontFamily "serif"
            ]
            [ text (String.fromInt score) ]
        ]


fibonacciView : Int -> List (Svg.Attribute Msg) -> Svg Msg
fibonacciView highestRank attrs =
    let
        numFibonacciRows =
            5

        rowSize =
            tileSpacing / (numFibonacciRows + 1)

        textSize =
            0.7 * rowSize

        numFibonacciCols =
            ceiling (toFloat highestRank / numFibonacciRows)

        colSize =
            panelWidth / toFloat (Basics.max 3 numFibonacciCols)
    in
    g
        attrs
        (text_
            [ x "0"
            , y (String.fromFloat rowSize)
            , fontSize (String.fromFloat textSize)
            , fontFamily "sans-serif"
            , fontWeight "bold"
            ]
            [ text "Fibonacci numbers" ]
            :: List.map
                (\rank ->
                    let
                        col =
                            rank // numFibonacciRows

                        row =
                            remainderBy numFibonacciRows rank
                    in
                    text_
                        [ x (String.fromFloat (colSize * toFloat (col + 1)))
                        , y (String.fromFloat (rowSize * toFloat (row + 2)))
                        , textAnchor "end"
                        , fontSize (String.fromFloat textSize)
                        , fontFamily "sans-serif"
                        , fontWeight "bold"
                        ]
                        [ text (String.fromInt (fibonacci rank)) ]
                )
                (List.range 0 highestRank)
        )


tilesView : BoardState -> List (Svg Msg)
tilesView boardState =
    case boardState of
        Static { tiles } ->
            Grid.indexedFilterToList
                (\col row ->
                    Maybe.map
                        (\rank ->
                            tileView
                                (toFloat col)
                                (toFloat row)
                                (fibonacci rank)
                        )
                )
                tiles

        Sliding { tiles, amount, direction } ->
            Grid.indexedFilterToList
                (\col row ->
                    Maybe.map
                        (\{ label, isMoving } ->
                            if not isMoving then
                                tileView (toFloat col) (toFloat row) label

                            else
                                let
                                    ( dx, dy ) =
                                        directionToVector direction
                                in
                                tileView
                                    (toFloat col + dx * amount)
                                    (toFloat row + dy * amount)
                                    label
                        )
                )
                (withTransformedTiles direction convertToDisplayTiles tiles)


tileView : Float -> Float -> Int -> Svg Msg
tileView col row label =
    let
        labelSize =
            tileSize / 2.0
    in
    g
        [ transform
            ("translate("
                ++ String.fromFloat (tileMargin + col * tileSpacing)
                ++ ","
                ++ String.fromFloat (tileMargin + row * tileSpacing)
                ++ ")"
            )
        ]
        [ use [ href "#tileShadow" ] []
        , rect
            [ fill "url(#tileVerticalShading)"
            , x "0"
            , y "0"
            , width (String.fromFloat tileSize)
            , height (String.fromFloat tileSize)
            , rx (String.fromFloat tileCornerRadius)
            ]
            []
        , use [ href "#tileShading" ] []
        , text_
            [ x (String.fromFloat (tileSize / 2.0))
            , y (String.fromFloat (tileSize / 2.0 + labelSize * 0.35))
            , fill "#334833"
            , textAnchor "middle"
            , fontSize (String.fromFloat labelSize)
            , fontFamily "serif"
            , fontWeight "bold"
            ]
            [ text (String.fromInt label) ]
        ]


directionToVector : Direction -> ( Float, Float )
directionToVector direction =
    case direction of
        Up ->
            ( 0.0, -1.0 )

        Down ->
            ( 0.0, 1.0 )

        Left ->
            ( -1.0, 0.0 )

        Right ->
            ( 1.0, 0.0 )


gameOverView : Svg Msg
gameOverView =
    let
        bigTextSize =
            0.75 * tileSize

        smallTextSize =
            0.2 * tileSize
    in
    g
        []
        [ rect
            [ fill "white"
            , opacity "0.5"
            , x (String.fromFloat -boardMargin)
            , y (String.fromFloat -boardMargin)
            , width (String.fromFloat (boardMargin + viewWidth))
            , height (String.fromFloat (boardMargin + viewHeight))
            ]
            []
        , text_
            [ x (String.fromFloat (boardSize / 2.0))
            , y (String.fromFloat (boardSize / 2.0 + bigTextSize / 2.0))
            , textAnchor "middle"
            , fontSize (String.fromFloat bigTextSize)
            , fontFamily "sans-serif"
            , fontWeight "bold"
            ]
            [ text "Game Over" ]
        , text_
            [ x (String.fromFloat (boardSize / 2.0))
            , y
                (String.fromFloat
                    (boardSize / 2.0 + bigTextSize / 2.0 + smallTextSize)
                )
            , textAnchor "middle"
            , fontSize (String.fromFloat smallTextSize)
            , fontFamily "sans-serif"
            ]
            [ text "Press any key to start again" ]
        ]


viewWidth : Float
viewWidth =
    boardMargin + boardSize + panelMargin + panelWidth


viewHeight : Float
viewHeight =
    boardMargin + boardSize + boardShadowExtension + 2.0 * boardDepth


tileSpacing : Float
tileSpacing =
    150.0


tileMargin : Float
tileMargin =
    20.0


tileSize : Float
tileSize =
    tileSpacing - tileMargin


tileColor : Color
tileColor =
    Color.rgb 0.5 1.0 0.7


tileShadowExtension : Float
tileShadowExtension =
    0.5 * tileDepth


slotColor : Color
slotColor =
    Color.rgb 0.75 0.75 0.75


tileCornerRadius : Float
tileCornerRadius =
    15.0


tileDepth : Float
tileDepth =
    8.0


boardSize : Float
boardSize =
    tileMargin + toFloat numRows * tileSpacing


boardDepth : Float
boardDepth =
    0.5 * tileMargin


boardShadowExtension : Float
boardShadowExtension =
    boardDepth


boardMargin : Float
boardMargin =
    tileMargin


panelWidth : Float
panelWidth =
    2.0 * tileSpacing


panelMargin : Float
panelMargin =
    2.0 * tileMargin


slideTime : Float
slideTime =
    -- In milliseconds
    650.0

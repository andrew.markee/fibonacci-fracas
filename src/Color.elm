module Color exposing
    ( Color
    , add
    , byteToHexString
    , interp
    , interpString
    , mul
    , rgb
    , rgba
    , toString
    )

import Array


type alias Color =
    { r : Float, g : Float, b : Float, a : Float }


rgb : Float -> Float -> Float -> Color
rgb r g b =
    { r = r, g = g, b = b, a = 1.0 }


rgba : Float -> Float -> Float -> Float -> Color
rgba r g b a =
    { r = r, g = g, b = b, a = a }


add : Color -> Color -> Color
add color0 color1 =
    { r = color0.r + color1.r
    , g = color0.g + color1.g
    , b = color0.b + color1.b
    , a = (color0.a + color1.a) / 2.0
    }


mul : Float -> Color -> Color
mul factor color =
    { r = clamp (factor * color.r)
    , g = clamp (factor * color.g)
    , b = clamp (factor * color.b)
    , a = color.a
    }


clamp : Float -> Float
clamp comp =
    max 0.0 (min 1.0 comp)


interp : Color -> Color -> Float -> Color
interp color0 color1 dist =
    let
        clampedDist =
            max 0.0 (min 1.0 dist)
    in
    { r = interpComponent color0.r color1.r clampedDist
    , g = interpComponent color0.g color1.g clampedDist
    , b = interpComponent color0.b color1.b clampedDist
    , a = interpComponent color0.a color1.a clampedDist
    }


interpComponent : Float -> Float -> Float -> Float
interpComponent value0 value1 dist =
    value0 * (1.0 - dist) + value1 * dist


interpString : Color -> Color -> Float -> String
interpString color0 color1 dist =
    toString (interp color0 color1 dist)


toString : Color -> String
toString color =
    "#"
        ++ componentToHexString color.r
        ++ componentToHexString color.g
        ++ componentToHexString color.b
        ++ componentToHexString color.a


componentToHexString : Float -> String
componentToHexString x =
    byteToHexString (round (255.0 * x))


byteToHexString : Int -> String
byteToHexString byte =
    let
        clampedByte =
            max 0 (min 255 byte)
    in
    hexDigit (clampedByte // 16) ++ hexDigit (remainderBy 16 clampedByte)


hexDigit : Int -> String
hexDigit value =
    Array.get value (Array.fromList (String.toList "0123456789ABCDEF"))
        |> Maybe.map String.fromChar
        |> Maybe.withDefault ""
